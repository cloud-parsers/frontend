const { freeze } = Object;
const queryError = document.getElementById("query-error");
const queryField = document.getElementById("query");
const results = document.getElementById("results");

const FILE_TYPES = freeze({
  PDF: "pdf",
  PPTX: "pptx",
  IMG: "img",
});

const getSvgAsset = (name) => `assets/${name}.svg`;

const FILE_TYPE_LOGO_MAP = Object.freeze({
  [FILE_TYPES.PDF]: getSvgAsset("pdf-icon"),
  [FILE_TYPES.PPTX]: getSvgAsset("pptx-icon"),
  [FILE_TYPES.IMG]: getSvgAsset("img-icon"),
});

const FILE_TYPE_URL_MAP = Object.freeze({
  [FILE_TYPES.PDF]: "pdfParserAddress",
  [FILE_TYPES.PPTX]: "pptxParserAddress",
  [FILE_TYPES.IMG]: "imgParserAddress",
});

function setResults(files, fileType) {
  let html = "";
  for (const file of files) {
    html += `
      <li>
        <a class="file" href="${file.url}" target="_blank" rel="noopener noreferrer">
          <img src="${FILE_TYPE_LOGO_MAP[fileType]}" />
          <div class="textin">
          <p class="main">${file.name}</p>
          
          `;
    switch (fileType) {
      case "pptx":
        if (file.slide_path != null && file.slide_path != undefined)
          html += `
            <p class="ellipsis" data-text="${file.slide_path}">${file.slide_path}</p>
          `;
        break;
      case "img":
        if (file.text != null && file.text != undefined)
          html += `
              <p class="ellipsis" data-text="${file.text}">${file.text}</p>
            `;
      case "pdf":
        if (file.founded_line != null && file.founded_line != undefined)
          html += `
              <p class="ellipsis" data-text="${file.founded_line}">${file.founded_line}</p>
              
            `;
      default:
        break;
    }

    html += `
          </div>
        </a>
      </li>`;
  }

  results.innerHTML = html;
}

async function searchFilesAsync(fileType, query) {
  const params = new URLSearchParams({ query });

  const url = FILE_TYPE_URL_MAP[fileType];

  if (!url) return;
  const response = await fetch(url + "?" + params, {
    mode: "cors",
  });

  const data = await response.json();

  return data.files || [];
}

document.forms[0].addEventListener("submit", (e) => {
  e.preventDefault();

  const query = queryField.value.trim();

  if (!query) {
    queryField.classList.add("error");
    queryError.style.display = "block";
    return;
  }

  const fileType = document.getElementById("type").value;

  results.style.display = "flex";
  results.innerHTML = '<span class="loader"></span>';

  searchFilesAsync(fileType, query)
    .then((files) => {
      if (!files?.length) {
        results.innerHTML =
          '<li class="not-found">По вашому запиту нічого не знайдено :(</li>';
        return;
      }

      setResults(files, fileType);
      Array.from(document.getElementsByClassName("ellipsis")).forEach(function (
        item
      ) {
        if (checkEllipsis(item)) item.title = item.innerText;
      });
    })
    .catch(console.error);
});

function checkEllipsis(el) {
  const styles = getComputedStyle(el);
  const widthEl = parseFloat(styles.width);
  const ctx = document.createElement("canvas").getContext("2d");
  ctx.font = `${styles.fontSize} ${styles.fontFamily}`;
  const text = ctx.measureText(el.innerText);
  return text.width > widthEl;
}

queryField.addEventListener("keydown", () => {
  queryField.classList.remove("error");
  queryError.style.display = "none";
});
